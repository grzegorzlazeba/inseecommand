<?php

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Class ServicesTest
 *
 */
class ServicesTest extends KernelTestCase
{

	/** @test */
	public function a_last_commit_sha_is_returned()
	{
		$app = new Application( static::createKernel() );
		$cmd = $app->find( 'repo_sha' );
		$cmdTest = new CommandTester( $cmd );
		$cmdTest->execute(array(
			'command' => 'repo_sha',
			'repo_owner' => 'facebook/react',
			'branch'     => 'master'
		));
		$output = $cmdTest->getDisplay();
		$this->assertEquals(!preg_match('/[^a-z0-9]/', trim($output)), true );
	}

	/** @test */
	public function a_last_commit_sha_is_returned_with_github_option()
	{
		$app = new Application( static::createKernel() );
		$cmd = $app->find( 'repo_sha' );
		$cmdTest = new CommandTester( $cmd );
		$cmdTest->execute(array(
			'command' => 'repo_sha',
			'repo_owner' => 'facebook/react',
			'branch'     => 'master',
			'--service'     => 'github',
		));
		$output = $cmdTest->getDisplay();
		$this->assertEquals(!preg_match('/[^a-z0-9]/', trim($output)), true );
	}

	/** @test */
	public function error_is_displayed_when_service_not_found()
	{
		$app = new Application( static::createKernel() );
		$cmd = $app->find( 'repo_sha' );
		$cmdTest = new CommandTester( $cmd );
		$cmdTest->execute(array(
			'command' => 'repo_sha',
			'repo_owner' => 'facebook/react',
			'branch'     => 'master',
			'--service'     => 'test_____test',
		));
		$output = $cmdTest->getDisplay();
		$this->assertEquals(trim($output), 'Service not found');
	}

	/** @test */
	public function error_is_displayed_when_repository_not_found()
	{
		$app = new Application( static::createKernel() );
		$cmd = $app->find( 'repo_sha' );
		$cmdTest = new CommandTester( $cmd );
		$cmdTest->execute(array(
			'command' => 'repo_sha',
			'repo_owner' => 'test_____test',
			'branch'     => 'master'
		));

		$output = $cmdTest->getDisplay();
		$this->assertEquals(trim($output), 'Not Found');
	}

	/** @test */
	public function error_is_displayed_when_branch_not_found()
	{
		$app = new Application( static::createKernel() );
		$cmd = $app->find( 'repo_sha' );
		$cmdTest = new CommandTester( $cmd );
		$cmdTest->execute(array(
			'command' => 'repo_sha',
			'repo_owner' => 'facebook/react',
			'branch'     => 'test_____test'
		));

		$output = $cmdTest->getDisplay();
		$this->assertEquals(trim($output), 'Branch not found');
	}
}
