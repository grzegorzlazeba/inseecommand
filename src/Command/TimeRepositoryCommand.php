<?php

namespace App\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class TimeRepositoryCommand extends RepositoryCommand
{
	protected static $defaultName = 'repo_sha';
	private $services = [ 'github' ];
	// here you can add new services

	public function configure()
	{
		$this->addArgument(
						'repo_owner',
						InputArgument::REQUIRED,
						'Owner of repository' )
			 ->addArgument(
			 			'branch',
						InputArgument::REQUIRED,
				 	'Name of branch' )
			 ->addOption(
			 	'service',
				'--service',
				InputOption::VALUE_REQUIRED,
				'Choose one available service ['. implode( $this->services, ',' ).' ]' );

	}
	public function execute(InputInterface $input, OutputInterface $output)
	{
		$service = $input->getOption('service');
		if(is_null($service))
			$this->gitHub($input, $output);
		else {
			// here you can handle new services
			switch ($service) {
				case 'github' :
					$this->gitHub($input, $output);
					break;
				default:
					$this->notFoundService($input, $output);
			}
		}



	}
}
