<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command as SymfonyCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\ExceptionInterface;

class RepositoryCommand extends SymfonyCommand
{


	public function __construct()
	{
		parent::__construct();
	}


	protected function gitHub(InputInterface $input, OutputInterface $output)
	{

		$client = HttpClient::create();

		try {
			$response = $client->request(
				'GET',
				'https://api.github.com/repos/' . $input->getArgument('repo_owner') . '/branches/' . $input->getArgument('branch')
			);
			$statusCode = $response->getStatusCode();

		} catch (ExceptionInterface $e) {
			return $output->writeln($e->getMessage());
		}

		$content = $response->toArray(false); //don't throw the exception

		if ($statusCode !== 200)
			$output->writeln($content['message']);
		else
			$output->writeln($content['commit']['sha']);


	}

	protected function notFoundService(InputInterface $input, OutputInterface $output)
	{
		$output->writeln('Service not found');
	}
}
